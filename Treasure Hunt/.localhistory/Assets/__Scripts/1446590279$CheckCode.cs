﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

public class CheckCode : MonoBehaviour
{
    public Text Error;
    public CanvasGroup ErrorZone;

    public Text Title1;
    public Text Code1;
    public Text Title2;
    public Text Code2;
    public Text Title3;
    public Text Code3;
    public Text Title4;
    public Text Code4;
    public Text Title5;
    public Text Code5;
    public Text Title6;
    public Text Code6;

    public void OnClick()
    {
        Debug.Log("Clicked");

        if (Code1.text == "MZX563" && Code2.text == "JB007")
        {
            Debug.Log("Effects");
            Application.LoadLevel("Effects");
        }
        else
        {
            bool error = false;
            if (Code1.text == "MZX563")
                Code1.transform.parent.gameObject.SetActive(false);
            else if (Code1.text.Length > 0)
                error = true;

            if (Code2.text == "JB007")
                Code2.transform.parent.gameObject.SetActive(false);
            else if (Code2.text.Length > 0)
                error = true;

            if (Code2.text == "D2")
                Code2.transform.parent.gameObject.SetActive(false);
            else if (Code2.text.Length > 0)
                error = true;

            //&& Code2.text == "JB007")
            //Debug.Log("ERROR " + EneaCode.text);
            //ErrorZone.transform.parent.gameObject.SetActive(true);

            if (error)
                ErrorZone.DOFade(1, 3f).OnComplete(() =>
                {
                    ErrorZone.DOFade(0, 0);
                });

        }
    }
}
