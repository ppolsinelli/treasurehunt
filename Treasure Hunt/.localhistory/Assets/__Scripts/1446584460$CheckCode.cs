﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

public class CheckCode : MonoBehaviour
{
    public Text EneaTitle;
    public Text EneaCode;

    public Text Error;
    public Image ErrorZone;
    
    public Text Title1;
    public Text Code1;
    public Text Title2;
    public Text Code2;
    public Text Title3;
    public Text Code3;
    public Text Title4;
    public Text Code4;
    public Text Title5;
    public Text Code5;
    
    public void OnClick()
    {
        Debug.Log("Clicked");

        if (EneaCode.text == "MZX563")
        {
            Debug.Log("Effects");
            Application.LoadLevel("Effects");
        }
        else
        {
            Debug.Log("ERROR " + EneaCode.text);
            ErrorZone.gameObject.SetActive(true);

            ErrorZone.DOFade(1, 3f).OnComplete(() =>
            {
                ErrorZone.DOFade(0, 0);
                ErrorZone.gameObject.SetActive(false);
            });

        }
    }
}
