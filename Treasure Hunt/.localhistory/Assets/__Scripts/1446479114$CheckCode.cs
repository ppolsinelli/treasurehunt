﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CheckCode : MonoBehaviour
{
    public Text EneaTitle;
    public Text EneaCode;
    
    public void OnClick()
    {
        Debug.Log("Clicked");

        if (EneaCode.text == "MZX563")
        {
            Debug.Log("Effects");
            Application.LoadLevel("Effects");
        }
        else
        {
            Debug.Log("ERROR " + EneaCode.text);
            EneaTitle.text = "ERROR!";
        }
    }
}
