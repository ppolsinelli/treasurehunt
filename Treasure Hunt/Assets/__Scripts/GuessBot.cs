﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine.UI;

public class GuessBot : MonoBehaviour
{
    public TextMeshProUGUI Question;
    public TextMeshProUGUI Hint;
    public InputField Answer;

    public Quiz Current;
    public static List<Quiz> Quizzes = new List<Quiz>();
    private int at;
    private int attempts;
    
    // Use this for initialization
    void Start()
    {
        Setup();
        Current = Quizzes[0];
        SetupQuestion();
    }

    public void SetupQuestion()
    {
        Question.text = "" + (at + 1) + "/" + Quizzes.Count + ": " + Current.Question;
        Hint.text = "";
        Answer.text = "";
        attempts = 0;
    }

    public void AttemptAnswer()
    {
        if (Answer.text.Trim().Length > 3)
        {
            if (Current.CorrectAnswers.Contains(Answer.text))
            {
                Debug.Log("found");
                at++;
                if (at < Quizzes.Count)
                {
                    Current = Quizzes[at];
                    SetupQuestion();
                } else 
                    Application.LoadLevel("Ink");
            }
            else
            {
                attempts++;
                if (attempts > 1)
                {
                    int index = attempts - 2;
                    if (index < Current.Hints.Length)
                    {
                        Hint.text = Current.Hints[index];
                    }
                    else
                    {
                        Hint.text = Current.Hints[Current.Hints.Length-1]; 
                    }
                }
            }
        }
        Answer.text = "";
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Setup()
    {
        {
            Quiz qz1 = new Quiz();
            qz1.Question = "Due formiche vorrebbero scalare una matita, " +
                           "inizia la prima.. sale sale ma non ci riesce," +
                           " la seconda invece si...pero' salta in aria, come mai?";
            qz1.CorrectAnswers = new string[]
            {
                "mina"
            };
            qz1.Hints = new string[] { "E' saltata su ...","E' esplosa perche' ..." };
            Quizzes.Add(qz1);
        }
        {
            Quiz qz2 = new Quiz();
            qz2.Question = "Le dita del piede fanno una gara di corsa..sai perche' vince sempre il mignolo?";
            qz2.CorrectAnswers = new string[]
            {
                "LUCE", "alluce", "la luce", "lalluce", "più veloce della luce"
            };
            qz2.Hints = new string[] { "Faster than light" };
            Quizzes.Add(qz2);
        }
        {
            Quiz qz3 = new Quiz();
            qz3.Question = "Il farmacista, sua figlia, il pompiere e sua moglie vinsero alla lotteria " +
                           "e divisero il premio in tre parti, perche'?";
            qz3.CorrectAnswers = new string[]
            {
                "moglie=figlia","figlia=MOGLIE","moglie = figlia","figlia = moglie"
            };
            qz3.Hints = new string[] { "x = y" };
            Quizzes.Add(qz3);
        }
        {
            Quiz qz3 = new Quiz();
            qz3.Question = "Cosa porta Biancaneve in spiaggia?";
            qz3.CorrectAnswers = new string[]
            {
                "asciuganani","asciuganano","asciuga-nani","asciuga-nano","asciuga nani","asciuga nano"
            };
            qz3.Hints = new string[] { "asciuga-xxxx" };
            Quizzes.Add(qz3);
        }
        {
            Quiz qz = new Quiz();
            qz.Question = "Cosa porta Biancaneve in spiaggia?";
            qz.CorrectAnswers = new string[]
            {
                "asciuganani","asciuganano","asciuga-nani","asciuga-nano","asciuga nani","asciuga nano"
            };
            qz.Hints = new string[] { "asciuga-xxxx" };
            Quizzes.Add(qz);
        }

        {
            Quiz qz = new Quiz();
            qz.Question = "Cosa dice un panino a suo papa'?";
            qz.CorrectAnswers = new string[]
            {
                "kebab"
            };
            qz.Hints = new string[] { "arabo ...", "panino arabo ..." };
            Quizzes.Add(qz);
        }

    }

}
